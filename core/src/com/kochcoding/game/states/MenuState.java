package com.kochcoding.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.kochcoding.game.RocketPursuit;
import com.kochcoding.game.sprites.Background;
import com.kochcoding.game.sprites.Button;


public class MenuState extends State{
    private Background background;
    private Texture gamelabel;
    private Button playbtn;
    private Button settingsbtn;

    public MenuState(GameStateManager gsm) {
        super(gsm);
        cam.setToOrtho(false, RocketPursuit.WIDTH, RocketPursuit.WIDTH * ((float) Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth()));
        background = new Background("background.png");
        gamelabel = new Texture("gamelabel.png");
        playbtn = new Button("playbtn.png",new Vector2(0,0));
        playbtn.setPosition(new Vector2(60, cam.viewportHeight - 30 - gamelabel.getHeight() - 90 - playbtn.getTexture().getHeight()));
        settingsbtn = new Button("settingsbtn.png", new Vector2(0,0));
        settingsbtn.setPosition(new Vector2(cam.viewportWidth - playbtn.getTexture().getWidth() - 60, cam.viewportHeight - 30 - gamelabel.getHeight() - 90 - playbtn.getTexture().getHeight()));
    }

    @Override
    public void handleInput() {
        if(Gdx.input.justTouched()) {
            if(playbtn.isTouched((Gdx.input.getX() / (float) Gdx.graphics.getWidth()) * cam.viewportWidth, (Gdx.input.getY() / (float) Gdx.graphics.getHeight()) * cam.viewportHeight)) {
                gsm.set(new PlayState(gsm));
                dispose();
            }
            else if(settingsbtn.isTouched((Gdx.input.getX() / (float) Gdx.graphics.getWidth()) * cam.viewportWidth, (Gdx.input.getY() / (float) Gdx.graphics.getHeight()) * cam.viewportHeight)) {
                gsm.set(new SettingsState(gsm));
                dispose();
            }
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
        background.update(dt);
    }

    @Override
    public void render(SpriteBatch sb) {
        cam.update();
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        background.render(sb);
        sb.draw(gamelabel, cam.viewportWidth / 2 - gamelabel.getWidth() / 2, cam.viewportHeight - gamelabel.getHeight() - 30);
        sb.draw(playbtn.getTexture(), playbtn.getPosition().x, playbtn.getPosition().y);
        sb.draw(settingsbtn.getTexture(), settingsbtn.getPosition().x, settingsbtn.getPosition().y);
        sb.end();
    }

    @Override
    public void dispose() {
        gamelabel.dispose();
        playbtn.dispose();
        settingsbtn.dispose();
    }
}
