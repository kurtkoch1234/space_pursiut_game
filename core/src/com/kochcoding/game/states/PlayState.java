package com.kochcoding.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.math.Vector2;
import com.kochcoding.game.RocketPursuit;
import com.kochcoding.game.sprites.Background;
import com.kochcoding.game.sprites.Button;
import com.kochcoding.game.sprites.Comet;
import com.kochcoding.game.sprites.Rocket;


public class PlayState extends State {

    // Bitmap stuff
    private BitmapFont bitmap;
    float playedTime;
    private String scoreText;

    private Button pausebtn;
    private Background background;
    private Rocket rocket;
    private Comet comet;


    protected PlayState(GameStateManager gsm) {
        super(gsm);
        bitmapInit();
        cam.setToOrtho(false, RocketPursuit.WIDTH, RocketPursuit.WIDTH * ((float) Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth()));
        background = new Background("background.png");
        pausebtn = new Button("pausebtn.png", new Vector2(0,0));
        rocket = new Rocket("rocket.png", new Vector2((cam.viewportWidth-90)/2, 250));
        pausebtn.setPosition(new Vector2(RocketPursuit.WIDTH - pausebtn.getTexture().getWidth(), cam.viewportHeight - pausebtn.getTexture().getHeight()));
        comet = new Comet();
    }

    @Override
    protected void handleInput() {
        if(Gdx.input.justTouched()) {
            if(pausebtn.isTouched((Gdx.input.getX() / (float) Gdx.graphics.getWidth()) * cam.viewportWidth, (Gdx.input.getY() / (float) Gdx.graphics.getHeight()) * cam.viewportHeight)) {
                gsm.set(new MenuState(gsm));
                dispose();
            }
        }
        float accel=Gdx.input.getAccelerometerX();
        rocket.setAccelXInput(accel);
//        System.out.println(accel);
    }

    @Override
    public void update(float dt) {
        handleInput();
        background.update(dt);
        rocket.update(dt);
        comet.update(dt);
        playedTime += dt;
        scoreText = "" + (((int) (playedTime*10 + 0.5))/ 10.0);
    }

    @Override
    public void render(SpriteBatch sb) {
        cam.update();
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        background.render(sb);
        pausebtn.render(sb);
        rocket.render(sb);
        comet.render(sb);
        bitmap.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        bitmap.draw(sb, scoreText, 10, cam.viewportHeight-20 );
        sb.end();
    }

    @Override
    public void dispose() {
        background.dispose();
        rocket.dispose();
        pausebtn.dispose();
    }

    private void bitmapInit() {
        playedTime = 0;
        scoreText = "" + playedTime;
        bitmap = new BitmapFont();
        bitmap.getData().setScale(5);
    }
}
