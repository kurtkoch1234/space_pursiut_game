package com.kochcoding.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.kochcoding.game.RocketPursuit;
import com.kochcoding.game.sprites.Background;
import com.kochcoding.game.sprites.Button;


public class SettingsState extends State {
    private Background background;

    private Button backbtn;

    protected SettingsState(GameStateManager gsm) {
        super(gsm);
        cam.setToOrtho(false, RocketPursuit.WIDTH, RocketPursuit.WIDTH * ((float) Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth()));
        background = new Background("background.png");
        backbtn = new Button("backbtn.png", new Vector2(0, 0));
        backbtn.setPosition(new Vector2(0, cam.viewportHeight - backbtn.getTexture().getHeight()));
    }

    @Override
    protected void handleInput() {
        if(Gdx.input.justTouched()) {
            if(backbtn.isTouched((Gdx.input.getX() / (float) Gdx.graphics.getWidth()) * cam.viewportWidth, (Gdx.input.getY() / (float) Gdx.graphics.getHeight()) * cam.viewportHeight)) {
                gsm.set(new MenuState(gsm));
                dispose();
            }
        }

    }

    @Override
    public void update(float dt) {
        handleInput();
        background.update(dt);
    }

    @Override
    public void render(SpriteBatch sb) {
        cam.update();
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        background.render(sb);
        backbtn.render(sb);
        sb.end();
    }

    @Override
    public void dispose() {
        backbtn.dispose();
    }
}
