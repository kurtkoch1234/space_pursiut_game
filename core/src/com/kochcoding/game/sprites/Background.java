package com.kochcoding.game.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.kochcoding.game.RocketPursuit;

public class Background extends GameObject {
    private static final Vector2 BACKGROUND_SPEED = new Vector2(3, 40);
    private Vector2 pos2;

    public Background(String path) {
        super(path, new Vector2(0,0));
        pos.set(new Vector2(-(txt.getWidth() - RocketPursuit.WIDTH),0));
        pos2 = new Vector2(new Vector2(-(txt.getWidth() - RocketPursuit.WIDTH), txt.getHeight() - 10));
    }

    public void update(float dt) {
        pos.y -= dt * BACKGROUND_SPEED.y;
        pos.x += dt * BACKGROUND_SPEED.x;
        pos2.y -= dt * BACKGROUND_SPEED.y;
        pos2.x += dt * BACKGROUND_SPEED.x;

        if (pos.y <= -txt.getHeight()) {
            pos.y = txt.getHeight() - 30;
            pos.x = -(txt.getWidth() - RocketPursuit.WIDTH);
        } else if(pos2.y <= -txt.getHeight()) {
            pos2.y = txt.getHeight() - 30;
            pos2.x = -(txt.getWidth() - RocketPursuit.WIDTH);
        }
    }

    public Vector2 getPosition2() {
        return pos2;
    }

    public void render(SpriteBatch sb) {
        sb.draw(getTexture(), pos.x, pos.y);
        sb.draw(getTexture(), pos2.x, pos2.y);
    }


}
