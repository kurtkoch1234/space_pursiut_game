package com.kochcoding.game.sprites;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.kochcoding.game.RocketPursuit;


public class Comet extends GameObject {
    private static final Vector2 MAX_SPEED = new Vector2(50, 500);
    private static final Vector2 MIN_SPEED = new Vector2(3, 100);
    private static final float ROTATION_SPEED_MAX = 130;
    private static final int ROTATION_SPEED_MIN = 30;

    private int rotationSpeed;
    private float rotation;

    private Vector2 speed;

    //Constructor für zufälliges Bild, zufällige Position und zufälliger Geschwindigkeit
    public Comet() {
        super("comet" + (100 + (1 + (int)(Math.random() * 5))* 50) + ".png", new Vector2(-40 + (int)(Math.random() * RocketPursuit.WIDTH -40),RocketPursuit.WIDTH * ((float) Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth())));
        rotation = 0;
        rotationSpeed = ROTATION_SPEED_MIN + (int)(Math.random() * ROTATION_SPEED_MAX);
        speed = new Vector2(MIN_SPEED.x + (int)(Math.random() * MAX_SPEED.x), MIN_SPEED.y + (int)(Math.random() * MAX_SPEED.y));
        if(Math.round(Math.random()) == 1)
            speed.x = -speed.x;

    }

    //Constructor für bestimmtes Bild, zufällige Postion und zufällige Geschwindigkeit
    public Comet(int size) {
        super("comet"+size+".png", new Vector2(-40 + (int)(Math.random() * RocketPursuit.WIDTH -40),RocketPursuit.WIDTH * ((float) Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth())));
        rotation = 0;
        rotationSpeed = ROTATION_SPEED_MIN + (int)(Math.random() * ROTATION_SPEED_MAX);
        if(Math.round(Math.random()) == 1)
            rotationSpeed = -rotationSpeed;
        speed = new Vector2(MIN_SPEED.x + (int)(Math.random() * MAX_SPEED.x), MIN_SPEED.y + (int)(Math.random() * MAX_SPEED.y));
        if(Math.round(Math.random()) == 1)
            speed.x = -speed.x;
    }

    //Constructor für bestimmtes Bild, bestimmte Position und zufällige Geschwindigkeit
    public Comet(int size, int pos) {
        super("comet"+size+".png", new Vector2(pos,RocketPursuit.WIDTH * ((float) Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth())));
        rotation = 0;
        rotationSpeed = ROTATION_SPEED_MIN + (int)(Math.random() * ROTATION_SPEED_MAX);
        if(Math.round(Math.random()) == 1)
            rotationSpeed = -rotationSpeed;
        speed = new Vector2(MIN_SPEED.x + (int)(Math.random() * MAX_SPEED.x), MIN_SPEED.y + (int)(Math.random() * MAX_SPEED.y));
        if(Math.round(Math.random()) == 1)
            speed.x = -speed.x;
    }

    //Contructor für bestimmtes Bild, bestimmte Position und bestimmte Geschwindigkeit
    public Comet(int size, int pos, Vector2 speed) {
        super("comet"+size+".png",  new Vector2(pos,RocketPursuit.WIDTH * ((float) Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth())));
        rotation = 0;
        rotationSpeed = ROTATION_SPEED_MIN + (int)(Math.random() * ROTATION_SPEED_MAX);
        if(Math.round(Math.random()) == 1)
            rotationSpeed = -rotationSpeed;
        this.speed = speed;
    }

    public void update(float dt) {
        pos.set(pos.x + speed.x * dt, pos.y - speed.y * dt);
        if(pos.y <= -txt.getHeight())
            txt.dispose();
        rotation += rotationSpeed * dt;
    }

    public void render(SpriteBatch sb) {
        sb.draw(new TextureRegion(txt), pos.x, pos.y, txt.getWidth()/2, txt.getHeight()/2, txt.getWidth(), txt.getHeight(), 1, 1, rotation);
    }
}
