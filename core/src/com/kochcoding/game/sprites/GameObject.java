package com.kochcoding.game.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class GameObject {
    protected Texture txt;
    protected Vector2 pos;

    public GameObject(String path, Vector2 position) {
        txt = new Texture(path);
        pos = new Vector2(position);
    }

    public void setPosition(Vector2 position) {
        pos.x = position.x;
        pos.y = position.y;
    }

    public Texture getTexture() {
        return txt;
    }

    public Vector2 getPosition() {
        return pos;
    }

    public void update(float dt) {}
    public void render(SpriteBatch sb) { sb.draw(txt, pos.x, pos.y); }
    public void dispose() { txt.dispose(); }
}
