package com.kochcoding.game.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.kochcoding.game.states.PlayState;

public class Rocket extends GameObject {
    private float accelXInput=0;
    private static final float accel_rocket_constant=30f;
    private float omega=0;
    private static final float mass=2f;

    public Rocket(String path_to_texture, Vector2 position) {
        super(path_to_texture,position);
    }

    public void setAccelXInput(float x) {
        if (x>1f)
            x=1f;
        x*=8f;
        accelXInput=x;
    }

    @Override
    public void update(float dt) {
        pos.x-=0.5*dt*dt*accelXInput*5000;

        // to prevent rocket runs out of screen
        if (pos.x>Gdx.graphics.getWidth())
            pos.x=0;
        else if (pos.x<0)
            pos.x=Gdx.graphics.getWidth();

        // calculate acceleration of rocket in y under assumption absolute value of it is fixed
        float accelRocket=(float) Math.sqrt(accel_rocket_constant*accel_rocket_constant-accelXInput*accelXInput);
        float omega_nan=(float) Math.toDegrees(Math.asin(accelXInput / (accelRocket-mass*9.81f)));
        if (! Float.isNaN(omega_nan))
            omega=omega_nan;
        // if omega_nan=NaN then is the power of the rocket to move along y-axis to weak and it can not
        // escape the gravity force

        // i.e. one could add a timer for how long it is allowed to fly like that
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.draw(txt,pos.x,pos.y,txt.getWidth()/2,0,txt.getWidth(),txt.getHeight(),1f,1f,omega,0,0,txt.getWidth(),txt.getHeight(),false,false);
    }
}
