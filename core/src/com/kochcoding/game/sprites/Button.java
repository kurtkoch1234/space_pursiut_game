package com.kochcoding.game.sprites;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.kochcoding.game.RocketPursuit;

public class Button extends GameObject {

    public Button(String path, Vector2 position){
        super(path, position);
    }

    public boolean isTouched(float x, float y) {
        if(x >= getPosition().x && x <= getPosition().x + txt.getWidth() && RocketPursuit.WIDTH * ((float) Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth()) - y >= getPosition().y && RocketPursuit.WIDTH * ((float) Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth()) - y <= getPosition().y + txt.getHeight()) {
            return true;
        }
        return false;
    }
}
